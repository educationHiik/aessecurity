/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aes;

import java.security.spec.KeySpec;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author vaganovdv
 */
public class AesEncryptor
{
    
    
    private static String salt      = "12345"; 

    public AesEncryptor()
    {
    }
    
    public String encrypt(String strToEncrypt, String secret)
    {
        try
        {
            
            byte[] iv =
            {
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
            };
            IvParameterSpec ivspec = new IvParameterSpec(iv);

            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            
            // Фомрирование 256 байтного секретного ключа на основе пароля + salt
            //                                  пароль   дополнительный пароль (salt)    к-во итераций алгоритма     длина ключа шифрования
            //                                   |                  |                       |                           |
            KeySpec spec = new PBEKeySpec(secret.toCharArray(), salt.getBytes(),           65536,                      256);
            SecretKey tmp = factory.generateSecret(spec);
            SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");

            //  Cipher Block Chaining (CBC)
            //  Режим сцепления блоков шифротекста
            
            
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivspec);
            byte [] secureBytes = cipher.doFinal(strToEncrypt.getBytes("UTF-8"));
            return Base64.getEncoder().encodeToString(secureBytes);
            
            
        } catch (Exception e)
        {
            System.out.println("Error while encrypting: " + e.toString());
        }
        return null;
    }
    
}
