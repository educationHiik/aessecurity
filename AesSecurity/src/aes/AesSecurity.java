/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aes;

/**
 *
 * @author vaganovdv
 */
public class AesSecurity
{

    private static String secretKey = "123";

    public static void main(String[] args)
    {
        String originalString = "Очень секретное сообщение";

        AesEncryptor encryptor = new AesEncryptor();
        AesDecryptor decryptor = new AesDecryptor();

        String encryptedString = encryptor.encrypt(originalString, secretKey);
        String decryptedString = decryptor.decrypt(encryptedString, secretKey);

        System.out.println(originalString);
        System.out.println(encryptedString);
        System.out.println(decryptedString);
    }

}
